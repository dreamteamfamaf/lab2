#include "types.h"
#include "user.h"
#include "semaphore.h"
#define SEM_BARRIER 0

int
main(void)
{
	if(sem_open(SEM_BARRIER, SEM_OPEN, 0) == SEM_NOTEXIST){
		printf(1, "La barrera se encuentra inactiva \n");
	}else{
		sem_up(SEM_BARRIER);
		sem_close(SEM_BARRIER);
	}
	exit();
}