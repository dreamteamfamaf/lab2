#include "types.h"
#include "defs.h"
#include "semaphore.h"
#include "spinlock.h"

// Constant that keeps track of semaphores
// -1 means NON_EXISTENT
// 	0 means EXIST
int semaphore[MAX];
int num_prosses[MAX];

struct spinlock semlock;

int
sem_open(int sem, int flag, int value)
{
  acquire(&semlock);
  switch(flag){
  	case SEM_CREAT:
      if(semaphore[sem] == SEM_NOTEXIST || sem > 14){
        semaphore[sem] = value;
        release(&semlock);
        return 0;
      } 
      release(&semlock);
			return -1;
		case SEM_OPEN:
      if(semaphore[sem] == SEM_EXIST){
        semaphore[sem] = value;
        release(&semlock);
        return 0;
      }
      release(&semlock);
			return -1;
    case SEM_OPEN_OR_CREAT:
      if(semaphore[sem] == SEM_NOTEXIST)
        semaphore[sem] = value;   
      else
      	semaphore[sem] = 1;
      release(&semlock);
      return 0;
    default:
      cprintf("Error, unknown flag\n");
      release(&semlock);
      return -1;
	}

	
}

int 
sem_close(int sem)
{
	acquire(&semlock);
	if (semaphore[sem] == SEM_NOTEXIST){
		cprintf("Error, unknown semaphore\n");
		release(&semlock);
		return -1;
	}

	int n = num_prosses[sem];
	if (n == SEM_EXIST)  //If there are no processes, but exist semaphore
		semaphore[sem] = -1;//The semaphore is released
	
	release(&semlock);
	return n;
}

int
sem_up(int sem)
{
	acquire(&semlock);
	if (semaphore[sem] == SEM_NOTEXIST){
		cprintf("Error, unknown semaphore\n");
		release(&semlock);
		return -1;
	}

	semaphore[sem]++; //Unlock the semaphore
	wakeup(&semaphore[sem]);

	release(&semlock);
	return 0;
}

int
sem_down(int sem)
{
	acquire(&semlock);
	if (semaphore[sem] == SEM_NOTEXIST){
		cprintf("Error, unknown semaphore\n");
		release(&semlock);
		return -1;
	}
	num_prosses[sem]++;
  while(semaphore[sem] == 0){
    sleep(&semaphore[sem], &semlock);
  }
	semaphore[sem]--;
	num_prosses[sem]--;

  

	release(&semlock);
	return 0;
}






















