#include "types.h"
#include "user.h"
#include "semaphore.h"
#define SEM_PING 0
#define SEM_PONG 1

int
main(int argc, char *argv[])
{
	if(sem_open(SEM_PING, SEM_OPEN, 1) == -1 && sem_open(SEM_PONG, SEM_OPEN, 0) == -1)
		printf(1, "La barrera se encuentra inactiva \n");
	else{
		int pid = fork();
		for(int i = 0; i < atoi(argv[1]); i++){ 
			if(pid == 0){
				sem_down(SEM_PONG);
				printf(1, "\t pong %d \n", pid);
				sem_up(SEM_PING);
			} else {
				sem_down(SEM_PING);
				printf(1, "ping %d\n", pid);
				sem_up(SEM_PONG);
			}
		}
		wait(); //Por que hace que no salga el zombie?
	}
	//Hay un error en las siguientes dos lineas. ARREGLENLO.
	sem_close(SEM_PING); // ACA
	sem_close(SEM_PONG); // y ACA
	//Se tienen que dar cuenta, sino les voy a dar con la chancleta :*
	exit();
}
